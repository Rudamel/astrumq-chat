var gulp = require('gulp');
var uglify = require('rollup-plugin-uglify');
var cssnano = require('gulp-cssnano');
var rollup = require('rollup').rollup;
var commonjs = require('rollup-plugin-commonjs');
var nodeResolve = require('rollup-plugin-node-resolve');
var buble = require('rollup-plugin-buble')
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('scripts', function () {
  rollup({
    entry: 'resources/assets/js/main.js',
    plugins: [
      nodeResolve({ jsnext: true }),
      commonjs(),
      buble(),
      uglify()
    ]
  }).then(function (bundle) {
    return bundle.write({
      format: 'iife',
      moduleName: 'MainBundle',
      dest: 'www/js/main.js'
    });
  });
});

gulp.task('styles', function() {
    gulp.src('resources/assets/sass/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(gulp.dest('./www/css'));

    gulp.src('./node_modules/bootstrap-sass/assets/fonts/**/*', {base: './node_modules/bootstrap-sass/assets/fonts/'})
        .pipe(gulp.dest('./www/fonts'));
});