export function invokeModal(dataUrl) {
    $.get(dataUrl)
        .done(function (data) {
            let modal = `<div class="modal fade userModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Profil</h4>
                        </div>
                        <div class="modal-body">
                            <h4 class="text-center"><strong>${data.email}</strong></h4>
                            <p><strong>Datum registrace</strong>: ${data.created_at}</p>
                            <p><strong>Počet zpráv</strong>: ${data.messagesCount}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-company center-block" data-dismiss="modal">Zavřít</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->`;

            $('#modal-placeholder').html(modal);

            $('.userModal').modal();
        });
}