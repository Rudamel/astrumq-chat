import './jquery-global';
import 'bootstrap-sass';
import { invokeModal } from './modal.js'

(function () {
    $('.load-profile').click(function (e) {
        e.preventDefault();

        invokeModal($(this).data('url'));
    });

    if ($('.message-form').length > 0) {
        $('html, body').animate({
            scrollTop: $(".message-form").offset().top
        }, 500);
    }
})();
