# **Instalační pokyny** #

* naklonujte si repozitář > git clone https://Rudamel@bitbucket.org/Rudamel/astrumq-chat.git
* nastavte správná přístupová práva pro složky log a temp > chmod -R a+rw temp log
* nainstalujte závislosti > composer install
* aplikace používá různé nodejs moduly pro správu assetů (gulp, rollup, sass, cssnano, uglify, ...) proto pokud byste chtěli upravovat js nebo css je třeba tyto moduly nainstalovat přikazem npm install toto ale není třeba tento repozitář obsahuje zkompilované všechny soubory nutné pro běh aplikace

## **Vytvoření databáze** ##
* vytvořte databázi (možno použít nástroj adminer na adrese localhost/adminer.php)
* zadejte přístupové údaje pro databázi do konfigurařních souborů (neon soubor pro nette framework a phinx.yml pro migrace)
* vytvořte tabulky pomocí migrací přikazem > php vendor/bin/phinx migrate
* nastavte databázové údaje pro MessageSeeder v metodě change() (database/seeds/MessageSeeder)
* použijte seedery pro vložení dummy dat > php vendor/bin/phinx seed:run -s UserSeeder && php vendor/bin/phinx seed:run -s MessageSeeder