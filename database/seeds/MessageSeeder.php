<?php

use Phinx\Seed\AbstractSeed;

class MessageSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::Create();

        $db = new PDO('mysql:host=localhost;dbname=astrumq;charset=utf8', 'root', 'root');
        $stmt = $db->query('select id from users');
        $ids = $stmt->fetchAll(PDO::FETCH_NUM);

        $data = [];

        for ($i = 0; $i < 20; $i++) {
            $data[] = [
                'message' => $faker->paragraph,
                'user_id' => $ids[array_rand($ids)][0]
            ];
        }

        $this->insert('messages', $data);
    }
}
