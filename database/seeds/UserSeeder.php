<?php

use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::Create();
        $data = [];

        for ($i = 0; $i < 10; $i++) {
            $data[] = [
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'password' => \Nette\Security\Passwords::hash('1234'),
                'email' => $faker->email
            ];
        }

        $this->insert('users', $data);
    }
}
