<?php

namespace App\AstrumQ\Forms;

use Nette\Application\UI;

class MessageForm
{
    /** @var Nette\Database\Context */
    private $database;

    public function __construct(\Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function create()
    {
        $form = new UI\Form;

        $form->addTextArea('message', 'Zpráva')
            ->setRequired('Zadejte Zprávu')
            ->addRule(UI\Form::FILLED, 'Zadejte Zprávu');

        $form->addSubmit('post', 'Přidat Zprávu');

        return $form;
    }

    public function postMessage($formValues, $userId)
    {
        $this->database->table('messages')->insert([
            'message' => $formValues->message,
            'user_id' => $userId
        ]);

        return true;
    }
}
