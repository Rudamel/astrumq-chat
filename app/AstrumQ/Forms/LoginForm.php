<?php

namespace App\AstrumQ\Forms;

use Nette\Application\UI;

class LoginForm
{
    /** @var Nette\Database\Context */
    private $database;

    public function __construct(\Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function create()
    {
        $form = new UI\Form;

        $form->addText('email', 'E-mail')
            ->setRequired('Zadejte E-mail')
            ->addRule(UI\Form::EMAIL, 'Zadejte platný email')
            ->addRule(UI\Form::FILLED, 'Zadejte E-mail');

        $form->addPassword('password', 'Heslo')
            ->setRequired('Zadejte heslo')
            ->addRule(UI\Form::FILLED, 'Zadejte heslo');

        $form->addCheckbox('remember', 'Zapamatovat');

        $form->addSubmit('login', 'Login');

        return $form;
    }
}
