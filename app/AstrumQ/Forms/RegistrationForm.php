<?php

namespace App\AstrumQ\Forms;

use Nette\Application\UI;

class RegistrationForm
{
    /** @var Nette\Database\Context */
    private $database;

    public function __construct(\Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function create()
    {
        $form = new UI\Form;

        $form->addText('name', 'Jméno')
            ->setRequired('Zadejte jméno')
            ->addRule(UI\Form::FILLED, 'Zadejte jméno');

        $form->addText('surname', 'Příjmení')
            ->setRequired('Zadejte příjmení')
            ->addRule(UI\Form::FILLED, 'Zadejte příjmení');

        $form->addText('email', 'E-mail')
            ->setRequired('Zadejte E-mail')
            ->addRule(UI\Form::EMAIL, 'Zadejte platný email')
            ->addRule(UI\Form::FILLED, 'Zadejte E-mail');

        $form->addPassword('password', 'Heslo')
            ->setRequired('Zadejte heslo')
            ->addRule(UI\Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků/y', 6)
            ->addRule(UI\Form::FILLED, 'Zadejte heslo');

        $form->addPassword('password_confirmation', 'Heslo pro kontrolu')
            ->setRequired('Zadejte prosím heslo ještě jednou pro kontrolu')
            ->addRule(UI\Form::EQUAL, 'Hesla se neshodují', $form['password'])
            ->addRule(UI\Form::FILLED, 'Zadejte prosím heslo ještě jednou pro kontrolu');

        $form->addSubmit('register', 'Registrovat');

        return $form;
    }

    public function registerUser($data)
    {
        $this->database->table('users')->insert([
            'name'      => $data->name,
            'surname'   => $data->surname,
            'email'     => $data->email,
            'password'  => \Nette\Security\Passwords::hash($data->password)
        ]);

        return true;
    }
}
