<?php

namespace App\AstrumQ\Security;

use Nette;
use Nette\Security as NS;

class DatabaseAuthenticator extends Nette\Object implements NS\IAuthenticator
{
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function authenticate(array $credentials)
    {
        $credentials = $credentials[0];

        $row = $this->database->table('users')
            ->where('email', $credentials['email'])->fetch();

        if (!$row) {
            throw new NS\AuthenticationException('Uživatel s tímto e-mailem neexistuje.');
        }

        if (!NS\Passwords::verify($credentials['password'], $row->password)) {
            throw new NS\AuthenticationException('Nesprávné heslo.');
        }

        return new NS\Identity($row->id, null, ['email' => $row->email, 'created_at' => $row->created_at]);
    }
}
