<?php

namespace App\AstrumQ\Repositories;

use Nette;

class MessagesRepository
{
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function getAll($orderByField, $orderDirection = 'ASC')
    {
        $orderFieldWithTable = ($orderByField == 'name') ? 'user.name' : 'created_at';

        return $this->database->table('messages')
            ->order(sprintf('%s %s', $orderFieldWithTable, $orderDirection));
    }

    public function getMessageCountForUser($userId)
    {
        return $this->database->table('messages')
            ->where('user_id', $userId)->count();
    }

    public function groupMessages($messages, $loggedUserId)
    {
        $groups = [];

        $lastPosterId = 0;

        foreach ($messages as $message) {
            if (empty($groups)) {
                $groups[] = $this->createGroup($message, $loggedUserId);
            }

            if ($message->user->id == $lastPosterId) {
                $this->addMessageToLastGroup($groups, $message);
            } else {
                $groups[] = $this->createGroup($message, $loggedUserId);
            }

            $lastPosterId = $message->user->id;
        }

        return $groups;
    }

    private function createGroup($message, $loggedUserId)
    {
        return [
            'user' => $message->user->email,
            'type' => ($message->user->id == $loggedUserId) ? 'self' : 'other',
            'messages' => [$message]
        ];
    }

    private function addMessageToLastGroup(&$groups, $message)
    {
        $index = $this->getLastGroupIndex($groups);
        $groups[$index]['messages'][] = $message;
    }

    private function getLastGroupIndex($groups)
    {
        end($groups);

        return key($groups);
    }
}
