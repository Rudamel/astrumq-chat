<?php

namespace App\Presenters;

use Nette;

class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var Nette\Database\Context */
    protected $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    protected function restrictGuests()
    {
        if ( ! $this->getUser()->isLoggedIn()) {
            $this->redirect('Auth:login');
        }
    }
}
