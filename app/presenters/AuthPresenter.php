<?php

namespace App\Presenters;

use Nette;

use App\AstrumQ\Forms;
use Nette\Security as NS;
use App\AstrumQ\Security;

class AuthPresenter extends BasePresenter
{
    protected function createComponentRegistrationForm()
    {
        $form = (new Forms\RegistrationForm($this->database))->create();
        $form->onSuccess[] = [$this, 'registerUser'];

        return $form;
    }

    public function registerUser($form, $values)
    {
        try {
            (new Forms\RegistrationForm($this->database))->registerUser($values);
        } catch (Exception $e) {
            $this->flashMessage('Vyskytla se chyba', 'alert-danger');
            $this->redirect('Auth:register');
        }

        $this->flashMessage('Registrace proběhla úspěšně', 'alert-success');
        $this->redirect('Auth:login');
    }

    protected function createComponentLoginForm()
    {
        $form = (new Forms\LoginForm($this->database))->create();
        $form->onSuccess[] = [$this, 'login'];

        return $form;
    }

    public function login($form, array $values)
    {
        try {
            $user = $this->getUser();
            $user->login($values);

            if ($values['remember']) {
                $user->setExpiration('10 days', false);
            } else {
                $user->setExpiration(0, true);
            }
        } catch (NS\AuthenticationException $e) {
            $this->flashMessage($e->getMessage(), 'alert-danger');
            $this->redirect('Auth:login');
        }

        $this->flashMessage('Přihlášení proběhlo úspěšně', 'alert-success');
        $this->redirect('Chat:default');
    }

    public function actionLogout()
    {
        $this->user->logout();

        $this->flashMessage('Odhlášení proběhlo úspěšně', 'alert-success');
        $this->redirect('Auth:login');
    }
}
