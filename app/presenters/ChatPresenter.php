<?php

namespace App\Presenters;

use Nette;
use App\AstrumQ\Forms;
use App\AstrumQ\Repositories;
use Nette\Application\Responses\JsonResponse;

class ChatPresenter extends BasePresenter
{
    protected $messagesRepository;
    protected $user;

    protected function startup()
    {
        $this->restrictGuests();

        $this->messagesRepository = new Repositories\MessagesRepository($this->database);
        $this->user = $this->getUser();

        parent::startup();
    }

    public function renderDefault($orderBy = 'created_at')
    {
        $messages = $this->messagesRepository->getAll($orderBy);
        $this->template->messageGroups = $this->messagesRepository->groupMessages($messages, $this->user->id);
    }

    protected function createComponentMessageForm()
    {
        $form = (new Forms\MessageForm($this->database))->create();
        $form->onSuccess[] = [$this, 'postMessage'];

        return $form;
    }

    public function postMessage($form, $values)
    {
        try {
            (new Forms\MessageForm($this->database))->postMessage($values, $this->user->id);
        } catch (Exception $e) {
            $this->flashMessage('Vyskytla se chyba', 'alert-danger');
            $this->redirect('Chat:');
        }

        $this->flashMessage('Zpráva uložena.', 'alert-success');
        $this->redirect('Chat:');
    }

    public function actionGetProfile($id)
    {
        $this->sendResponse(new JsonResponse([
            'email' => $this->user->getIdentity()->email,
            'created_at' => $this->user->getIdentity()->created_at->format('j. n. Y'),
            'messagesCount' => $this->messagesRepository->getMessageCountForUser($this->user->id)
        ]));
    }
}
