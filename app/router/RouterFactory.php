<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
        $router[] = new Route('register', 'Auth:register');
        $router[] = new Route('login', 'Auth:login');
        $router[] = new Route('logout', 'Auth:logout');
        $router[] = new Route('get-profile/<id>', 'Chat:getProfile');
        $router[] = new Route('<presenter>/<action> ? orderBy=<orderBy>');
        $router[] = new Route('<presenter>/<action>[/<id>]', 'Chat:default');

		return $router;
	}

}
